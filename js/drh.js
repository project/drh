(function ($) {

  Drupal.behaviors.drh = {

    attach: function (context, settings) {

      $('body').once('drh', function(){

        // Remove drh-void link functionallity
        $('a[href="' + settings.basePath + settings.pathPrefix + 'drh-void"], a[href="' + settings.basePath + '?q=' + settings.pathPrefix + 'drh-void"]').addClass('void-link');

        $('a.void-link, .void-link > a').click(function (event) {
          event.preventDefault();
        });

      });

      // Add target attribute to A elements.
      $("a[drh-target]").each(function () {
        $(this).attr('target', $(this).attr('drh-target')).removeAttr('drh-target');
      });
      $("a[drh_target]").each(function () {
        $(this).attr('target', $(this).attr('drh_target')).removeAttr('drh_target');
      });

    }

  }

  $.fn.drh_fix_boxes_heights = function(selector) {
    var $container = $(this);
    var $boxes = $(this).children(selector);

    $boxes.css('height', '');

    var row_id = new Date().getTime();
    var max_height = 0;
    var is_last = false;

    $boxes.each(function () {
      var $next = $(this).next();
      var box_class = 'drh-height-' + row_id;
      $(this).addClass(box_class);
      max_height = Math.max(max_height, $(this).height());

      if ($next.size() == 0 || $(this).offset().top != $next.offset().top) {
        is_last = true;
      }

      if (is_last) {
        $container.children('.' + box_class).height(max_height);
        row_id++;
        is_last = false;
        max_height = 0;
      }
    });
  }

})(jQuery);
