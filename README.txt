Dr. H (Drupal Helper) is a set of handy functions created during personal Drupal
development to avoid having to implement them again and again in every new site.

I have never been a good documentation writer but I have tried to do a good job
here. So please check the comments before each function to understand what the
function does.

If you think something can be improved (code, documentation, the module
itself...), please do not hesitate to open an issue and I'll check it ASAP.

CURRENT FUNCTIONS
-----------------

- drh_match_url: Checks if the current page matches with a specific pattern.
- drh_match_urls: Checks if the current page matches any of the specified
patterns.
- drh_user_has_role: Determines if a user has a specific role assigned.
- drh_get_contrast_color: Gives the contrast color (black or white) depending if
the given color is light or dark.
- drh_db_select_table: Executes db_select function on a given table with
specific options and optionally formats the results array in the way the
developer needs each result.
- drh_match_ip4: Checks if an IP matches with a specific pattern.
- drh_ip4_range: Returns the first and last IP in a range represented by a
dotted IPV4 with optional mask. The function returns the first and last IPs in
the range in IP and Long Integer format.

OTHER FUNCTIONALITY
-------------------

- <a> target attribute:
  As HTML validators do not accept "target" attribute in links, I added a
  JavaScript solution here.

  Instead of using the "target" attribute you can use "drh-target" or
  "drh_target" attributes, and once the website is loaded, a JavaScript script
  will create the "target" attribute for that <A> element with the value you
  added.

  - Input example:
    <a href="http://example.com" drh-target="_blank">Example</a>
  - Result:
    <a href="http://example.com" target="_blank">Example</a>

  It works because HTML validators do not use JavaScript.

- drh-void page:
  You can use this page as a link in a menu to make that item to be displayed
  but the link itself doesn't work.

- void-link class:
  This class can be added to any <a> tag or any element containing an <a> tag to
  disable its link.
